<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("page_model", "page");
	}

	public function index()
	{
		$data['title'] = "Home";
		$this->load_view('backend/home', $data);
	}

	function add(){
		$data['title'] = "add Home";
		$this->load_view('backend/home', $data);	
	}

	function do_add(){
		$this->page->insert();
		redirect("");
	}


	// function yg selalu ada di setiap controller
	private function load_view($content, $data=array(), $template="backend/template"){
		$data['content'] = $content;
		$this->load->view($template, $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */