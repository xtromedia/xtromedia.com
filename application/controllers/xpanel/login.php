<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("user_model", "user");
	}

	public function index()
	{
		$this->load->view("backend/login");
	}

	function add(){
		$data['title'] = "add Home";
		$this->load_view('backend/home', $data);	
	}

	function do_add(){
		$this->page->insert();
		redirect("");
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */