<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

	function insert()
	{
		// function insert yg selalu ada di setiap model

		$this->db->insert("user");

		return $this->db->insert_id();
	}

	function update(){
		// function update yg selalu ada di setiap model	
		$id = $this->input->post("id");
		$this->db->where("id_user", $id);
		$this->db->update("user");
	}

	function delete($id){
		$this->db->where("id_user", $id);
		$this->db->delete("user");
		// function delete yg selalu ada di setiap model	
	}

	function get($id){ //$id bisa berupa slug atau id
		// function yg selalu ada di setiap model
		if(is_numeric($id)){
			$this->db->where("id_user", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("user");

		return $data->row();
	}

	function get_list($order = "created", $sort="desc", $limit="", $offset=""){ //$id bisa berupa slug atau id
		// function yg selalu ada di setiap model
			$this->db->where("id_user", $id);
			$this->db->where("slug", $id);
		$data = $this->db->get("user");

		return $data->row();
	} 
}
