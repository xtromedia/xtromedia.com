<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page_model extends CI_Model {

	function insert()
	{
		// function insert yg selalu ada di setiap model

		$this->db->insert("page");

		return $this->db->insert_id();
	}

	function update(){
		// function update yg selalu ada di setiap model	
		$id = $this->input->post("id");
		$this->db->where("id_page", $id);
		$this->db->update("page");
	}

	function delete($id){
		$this->db->where("id_page", $id);
		$this->db->delete("page");
		// function delete yg selalu ada di setiap model	
	}

	function get($id){ //$id bisa berupa slug atau id
		// function yg selalu ada di setiap model
		if(is_numeric($id)){
			$this->db->where("id_page", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("page");

		return $data->row();
	}

	function get_list($order = "created", $sort="desc", $limit="", $offset=""){ //$id bisa berupa slug atau id
		// function yg selalu ada di setiap model
			$this->db->where("id_page", $id);
			$this->db->where("slug", $id);

		$data = $this->db->get("page");

		return $data->row();
	} 
}
