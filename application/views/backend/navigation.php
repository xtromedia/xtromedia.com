<div class="leftbar leftbar-close clearfix">
		<div class="admin-info clearfix">
			<div class="admin-thumb">
				<i class="icon-user"></i>
			</div>
			<div class="admin-meta">
				<ul>
					<li class="admin-username">Huma Prathama</li>
					<li><a href="#">Edit Profile</a></li>
					<li><a href="#">View Profile </a><a href="#"><i class="icon-lock"></i> Logout</a></li>
				</ul>
			</div>
		</div>
		<div class="left-nav clearfix">
			<div class="left-primary-nav">
				<ul id="myTab">
					<li><a href="#pages" class="icon-th-large" title="Pages"></a></li>
					<li><a href="#Portfolio" class="icon-th-large" title="Portfolio"></a></li>
					<li><a href="#contact" class="icon-th-large" title="Contact"></a></li>
				</ul>
			</div>
			<div class="responsive-leftbar">
				<i class="icon-list"></i>
			</div>
			<div class="left-secondary-nav tab-content">
				<div class="tab-pane" id="pages">
					<h4 class="side-head">Pages</h4>
					<ul id="nav" class="accordion-nav">
						<li><a href="form-elements.html"><i class="icon-list-alt"></i> Create New Page</a></li>
						<li><a href="form-components.html"><i class="icon-th"></i> Pages List </a></li>
					</ul>
				</div>
				<div class="tab-pane" id="portfolio">
					<h4 class="side-head">Portfolio</h4>
					<ul id="nav" class="accordion-nav">
						<li><a href="form-elements.html"><i class="icon-list-alt"></i> Create New Portfolio</a></li>
						<li><a href="form-components.html"><i class="icon-th"></i> Portfolio List </a></li>
					</ul>
				</div>
				<div class="tab-pane" id="contact">
					<h4 class="side-head">contact</h4>
					<ul id="nav" class="accordion-nav">
						<li><a href="form-elements.html"><i class="icon-list-alt"></i> List contact</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>