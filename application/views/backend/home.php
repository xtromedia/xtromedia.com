<div class="row-fluid">
	<div class="span12">
		<div class="content-widgets light-gray">
			<div class="widget-head blue">
				<h3>Basic Form Elements</h3>
			</div>
			<div class="widget-container">
				<form class="form-horizontal">
					<div class="control-group">
						<label class="control-label">Text Input</label>
						<div class="controls">
							<input type="text" placeholder="Text Input" class="span12">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Text Input with left stripe</label>
						<div class="controls">
							<input type="text" placeholder="Text Input" class="span12 left-stripe">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Password Input</label>
						<div class="controls">
							<input type="password" placeholder="Password" class="span12">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Disable Input</label>
						<div class="controls">
							<input type="text" value="Disable Input" disabled="disabled" class="span12">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Checkbox</label>
						<div class="controls">
							<label class="checkbox">
							<input type="checkbox" value="">
							Option one is this and that—be sure to include why it's great </label>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Radio</label>
						<div class="controls">
							<label class="radio">
							<input type="radio" name="optionsRadios" value="option1" checked>
							Option one is this and that—be sure to include why it's great </label>
							<label class="radio">
							<input type="radio" name="optionsRadios" value="option2">
							Option two can be something else and selecting it will deselect option one </label>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Textarea</label>
						<div class="controls">
							<textarea rows="3" class="span12"></textarea>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Checkbox - Inline</label>
						<div class="controls">
							<label class="checkbox inline">
							<input type="checkbox" value="option1">
							1 </label>
							<label class="checkbox inline">
							<input type="checkbox" value="option2">
							2 </label>
							<label class="checkbox inline">
							<input type="checkbox" value="option3">
							3 </label>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Text Input</label>
						<div class="controls">
							<input type="text" class="span12">
							<span class="help-inline">Inline help text</span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Text Input</label>
						<div class="controls">
							<input type="text" class="span12">
							<span class="help-block">A longer block of help text that breaks onto a new line and may extend beyond one line.</span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Default File Input</label>
						<div class="controls">
							<input name="" type="file" class="file-uniform">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Selectbox</label>
						<div class="controls">
							<select class="span12">
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Selectbox -Multiple</label>
						<div class="controls">
							<select multiple="multiple" class="span12">
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
							</select>
						</div>
					</div>
					<div class="form-actions">
						<button type="submit" class="btn btn-success">Save changes</button>
						<button type="button" class="btn">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>