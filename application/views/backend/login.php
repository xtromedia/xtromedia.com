<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Xtromedia Admin Dashboard</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Admin Panel">
<meta name="author" content="xtromedia.com">
<!-- styles -->
<link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/css/bootstrap-responsive.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.css">
<!--[if IE 7]>
            <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
        <![endif]-->
<link href="<?php echo base_url() ?>assets/css/styles.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/css/theme-fabrics.css" rel="stylesheet">

<!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/ie/ie7.css" />
        <![endif]-->
<!--[if IE 8]>
            <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/ie/ie8.css" />
        <![endif]-->
<!--[if IE 9]>
            <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/ie/ie9.css" />
        <![endif]-->
<link href="<?php echo base_url() ?>assets/css/aristo-ui.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/css/elfinder.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>
<!--fav and touch icons -->
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url() ?>assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url() ?>assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url() ?>assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url() ?>assets/ico/apple-touch-icon-57-precomposed.png">
<!--============j avascript===========-->
<script src="<?php echo base_url() ?>assets/js/jquery.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery-ui-1.10.1.custom.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.js"></script>
</head>
<body>
<div class="layout">
	<!-- Navbar================================================== -->
	<div class="navbar navbar-inverse top-nav">
		<div class="navbar-inner">
			<div class="container">
				<span class="home-link"><a href="#" class="icon-home"></a></span><a class="brand" href="#"><img src="<?php echo base_url() ?>assets/images/logo-xtromedia.png" width="160" alt="xtromedia"></a>
				<div class="btn-toolbar pull-right notification-nav">
					<div class="btn-group">
						<div class="dropdown">
							<a class="btn btn-notification"><i class="icon-reply"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<form class="form-signin">
			<h3 class="form-signin-heading">Please sign in</h3>
			<div class="controls input-icon">
				<i class=" icon-user-md"></i>
				<input type="text" class="input-block-level" placeholder="Email address">
			</div>
			<div class="controls input-icon">
				<i class=" icon-key"></i><input type="password" class="input-block-level" placeholder="Password">
			</div>
			<label class="checkbox">
			<input type="checkbox" value="remember-me"> Remember me </label>
			<button class="btn btn-success btn-block" type="submit">Sign in</button>
			<h4>Forgot your password ?</h4>
			<p>
				<a href="#">Click here</a> to reset your password.
			</p>
		</form>
	</div>
</div>
</body>
</html>