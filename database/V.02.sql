-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 23, 2013 at 01:33 AM
-- Server version: 5.5.9
-- PHP Version: 5.3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `xtromedia`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id_category` tinyint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` VALUES(1, 'cutoff');
INSERT INTO `category` VALUES(2, 'revision');
INSERT INTO `category` VALUES(3, 'continues');
INSERT INTO `category` VALUES(4, 'generic');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `id_contact` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `comment` text NOT NULL,
  `id_project` smallint(6) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id_contact`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='data kontak dari kostumer web' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `contact`
--


-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `id_menu` tinyint(3) NOT NULL AUTO_INCREMENT,
  `name_en` varchar(255) NOT NULL,
  `name_id` varchar(255) NOT NULL,
  `parent` tinyint(3) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keyword` text,
  `meta_description` text,
  `type` varchar(255) NOT NULL,
  `link_url` varchar(255) NOT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='data menu2 di web front' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `menu`
--


-- --------------------------------------------------------

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
CREATE TABLE IF NOT EXISTS `page` (
  `id_page` tinyint(3) NOT NULL AUTO_INCREMENT,
  `id_menu` tinyint(3) NOT NULL,
  `title_id` varchar(255) NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `id_user` tinyint(3) NOT NULL,
  `description_id` text NOT NULL,
  `description_en` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id_page`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `page`
--


-- --------------------------------------------------------

--
-- Table structure for table `partner`
--

DROP TABLE IF EXISTS `partner`;
CREATE TABLE IF NOT EXISTS `partner` (
  `id_partner` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `ym` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `gtalk` varchar(255) DEFAULT NULL,
  `note` text,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id_partner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='data partner kerjasama atau client' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `partner`
--


-- --------------------------------------------------------

--
-- Table structure for table `platform`
--

DROP TABLE IF EXISTS `platform`;
CREATE TABLE IF NOT EXISTS `platform` (
  `id_platform` tinyint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_platform`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='web/desktop/mobile/dll' AUTO_INCREMENT=5 ;

--
-- Dumping data for table `platform`
--

INSERT INTO `platform` VALUES(1, 'web-based');
INSERT INTO `platform` VALUES(2, 'desktop-based');
INSERT INTO `platform` VALUES(3, 'mobile-based');
INSERT INTO `platform` VALUES(4, 'GIS');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
CREATE TABLE IF NOT EXISTS `project` (
  `id_project` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `id_partner` int(9) NOT NULL,
  `id_platform` int(9) NOT NULL,
  `id_category` int(9) NOT NULL,
  `id_user_proposal` int(9) NOT NULL,
  `harga_proposal` varchar(255) NOT NULL,
  `file_proposal` varchar(255) DEFAULT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `harga_deal` varchar(255) DEFAULT NULL,
  `date_finish` date NOT NULL,
  `id_status_partner` tinyint(3) NOT NULL,
  `note` text,
  `link_file` varchar(255) DEFAULT NULL,
  `komisi_sales` varchar(255) DEFAULT NULL,
  `komisi_user` varchar(255) DEFAULT NULL,
  `status` enum('0','1') NOT NULL,
  `id_user_edit` int(9) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `id_parent` int(9) NOT NULL COMMENT 'id_project parent jika revisi dan sejenisnya',
  PRIMARY KEY (`id_project`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='detail project atau produk' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `project`
--


-- --------------------------------------------------------

--
-- Table structure for table `project_handler`
--

DROP TABLE IF EXISTS `project_handler`;
CREATE TABLE IF NOT EXISTS `project_handler` (
  `id_project_handler` int(9) NOT NULL AUTO_INCREMENT,
  `id_project` int(9) NOT NULL,
  `id_user` int(9) NOT NULL,
  `persen` tinyint(3) NOT NULL,
  `rupiah` varchar(255) NOT NULL COMMENT 'bulatkan turun',
  `description` text,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id_project_handler`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='yang menangani masing2 project' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `project_handler`
--


-- --------------------------------------------------------

--
-- Table structure for table `rekap_bulanan`
--

DROP TABLE IF EXISTS `rekap_bulanan`;
CREATE TABLE IF NOT EXISTS `rekap_bulanan` (
  `id_rekap_bulanan` int(9) NOT NULL AUTO_INCREMENT,
  `masuk` int(12) NOT NULL,
  `keluar` int(12) NOT NULL,
  `saldo` int(12) NOT NULL,
  `date` date NOT NULL,
  `modified` date NOT NULL,
  `note` varchar(255) NOT NULL,
  `id_transaksi` int(9) NOT NULL COMMENT 'transaksi terakhir yang mempengaruhi',
  PRIMARY KEY (`id_rekap_bulanan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `rekap_bulanan`
--


-- --------------------------------------------------------

--
-- Table structure for table `status_partner`
--

DROP TABLE IF EXISTS `status_partner`;
CREATE TABLE IF NOT EXISTS `status_partner` (
  `id` tinyint(3) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='client/informan/orang dalam/lintah darat';

--
-- Dumping data for table `status_partner`
--


-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

DROP TABLE IF EXISTS `testimonial`;
CREATE TABLE IF NOT EXISTS `testimonial` (
  `id_testimonial` int(9) NOT NULL AUTO_INCREMENT,
  `id_project` int(9) NOT NULL,
  `comment` text NOT NULL,
  `tgl` datetime NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id_testimonial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='testimonial dari client terhadap hasil akhir project' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `testimonial`
--


-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

DROP TABLE IF EXISTS `transaksi`;
CREATE TABLE IF NOT EXISTS `transaksi` (
  `id_transaksi` int(9) NOT NULL AUTO_INCREMENT,
  `type` enum('in','out') NOT NULL,
  `jenis` enum('pd','pj') NOT NULL COMMENT 'pd = produk, pj = projek',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `note` text,
  `id_jenis` varchar(255) NOT NULL,
  `id_modifier` int(9) NOT NULL,
  PRIMARY KEY (`id_transaksi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='transaksi masuk dan keluar dari kas xtromedia' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `transaksi`
--


-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` tinyint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `auth` enum('0','1','2') NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='user admin web xtromedia' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `user`
--

